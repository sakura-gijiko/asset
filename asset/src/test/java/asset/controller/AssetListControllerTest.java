package asset.controller;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import asset.entity.AssetEntity;
import asset.entity.IndicationEntity;
import asset.entity.UserEntity;
import asset.form.SearchForm;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AssetListControllerTest {

	private MockMvc mockMvc;

	@Autowired
	AssetListController target;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.standaloneSetup(target).build();
	}

	public static MockHttpSession getMockHttpSession(Map<String, Object> sessions) {
		MockHttpSession mockHttpSession = new MockHttpSession();
		for (Map.Entry<String, Object> session : sessions.entrySet()) {
			mockHttpSession.setAttribute(session.getKey(), session.getValue());
		}
		return mockHttpSession;
	}

	@Test
	@Transactional
	public void 資産情報一覧画面初期表示() throws Exception {
		UserEntity user = new UserEntity();
		user.setUserId(1);
		user.setPassword("test");

		LinkedHashMap<String, Object> sessionMap = new LinkedHashMap<String, Object>() {
			{
				put("user", user);
			}
		};

		MockHttpSession mockSession = getMockHttpSession(sessionMap);
		// when
		MvcResult result = mockMvc.perform(get("/asset/list").session(mockSession)).andExpect(status().isOk())
				.andExpect(view().name("assetList")).andReturn();

		List<AssetEntity> assetResult = (List<AssetEntity>) result.getModelAndView().getModel().get("asset");
		IndicationEntity indicationResult = (IndicationEntity) result.getModelAndView().getModel()
				.get("indication");
		SearchForm searchResult = (SearchForm) result.getModelAndView().getModel().get("search");
		assertEquals(assetResult.get(0).getAssetId(), 1);
		assertEquals(indicationResult.isAssetNum(), true);
		assertEquals(searchResult.getAssetNum(), "");
	}

}
