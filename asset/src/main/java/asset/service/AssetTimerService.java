package asset.service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import asset.entity.AssetEntity;
import asset.repository.AssetRepository;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * レンタル終了予定日の延長に関するサービスクラス
 * @author s_nakano
 *
 */
@Slf4j
@Service
public class AssetTimerService {
	//資産情報タイマーの実行状態をチェックするための変数
	static @Getter private boolean assetTimerActive = false;
	@Autowired
	AssetRepository assetRepository;

	/**
	 * プロパティで指定した定期で、レンタル終了予定日を延長の値に応じて更新する
	 */
	@Scheduled(cron = "${cron.cronDefault}")
	public void assetTimer() {
		try {
			//資産情報タイマーの実行状態チェック用の変数にtrueを格納する
			assetTimerActive = true;

			//DBから延長が0以上で、レンタル終了予定日が現在日より前のものを取得
			List<AssetEntity> assetList = assetRepository.findByExtensionGreaterThanAndEndDateLessThan(0,
					LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd")));

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

			for (AssetEntity asset : assetList) {
				//LocalDateTimeのplusMonthsを実行するため、レンタル終了予定日をLocalDateTime型に変換
				Date formatDate = sdf.parse(asset.getEndDate());
				LocalDateTime d = LocalDateTime.ofInstant(formatDate.toInstant(), ZoneId.systemDefault());

				//plusMonthsで延長の値分、レンタル終了予定日を延長
				d = d.plusMonths(asset.getExtension());

				asset.setEndDate(d.format(DateTimeFormatter.ofPattern("yyyy/MM/dd")));
				asset.setExtension(0);
				assetRepository.save(asset);
			}

		} catch (Exception e) {
			log.error("資産情報タイマー処理エラー発生：", e);
		} finally {
			assetTimerActive = false;
		}
	}
}
