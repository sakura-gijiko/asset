package asset.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@Slf4j
public class AssetAspect {

	@Before("execution(* asset.controller.LoginController.login(..))")
	public void LoginLogBefore(JoinPoint joinPoint) {
		log.info("ログイン画面遷移処理開始：" + joinPoint.getSignature());
	}

	@AfterReturning("execution(* asset.controller.LoginController.login(..))")
	public void LoginLogAfterReturning(JoinPoint joinPoint) {
		log.info("ログイン画面遷移正常終了：" + joinPoint.getSignature());
	}

	@AfterThrowing(value = "execution(* asset.controller.LoginController.login(..))", throwing = "e")
	public void LoginLogAfterThrowing(JoinPoint joinPoint, Throwable e) {
		log.error("ログイン画面遷移エラー発生：", e);
		log.error("Signature：" + joinPoint.getSignature());
	}

	@Before("execution(* asset.controller.LoginController.check(..))")
	public void LoginCheckLogBefore(JoinPoint joinPoint) {
		log.info("ログイン処理処理開始：" + joinPoint.getSignature());
	}

	@AfterReturning("execution(* asset.controller.LoginController.check(..))")
	public void LoginCheckLogAfterReturning(JoinPoint joinPoint) {
		log.info("ログイン処理正常終了：" + joinPoint.getSignature());
	}

	@AfterThrowing(value = "execution(* asset.controller.LoginController.check(..))", throwing = "e")
	public void LoginCheckLogAfterThrowing(JoinPoint joinPoint, Throwable e) {
		log.error("ログイン処理エラー発生：", e);
		log.error("Signature：" + joinPoint.getSignature());
	}

	@Before("execution(* asset.repository.UserRepository.getOne(..))")
	public void LoginRepositoryGetOneLogBefore(JoinPoint joinPoint) {
		log.info("ユーザ情報取得処理開始：" + joinPoint.getSignature());
	}

	@AfterReturning("execution(* asset.repository.UserRepository.getOne(..))")
	public void LoginRepositoryGetOneLogAfterReturning(JoinPoint joinPoint) {
		log.info("ユーザ情報取得正常終了：" + joinPoint.getSignature());
	}

	@AfterThrowing(value = "execution(* asset.repository.UserRepository.getOne(..))", throwing = "e")
	public void LoginRepositoryGetOneLogAfterThrowing(JoinPoint joinPoint, Throwable e) {
		log.error("ユーザ情報取得エラー発生：", e);
		log.error("Signature：" + joinPoint.getSignature());
	}

	@Before("execution(* asset.controller.AssetListController.list(..))")
	public void AssetListLogBefore(JoinPoint joinPoint) {
		log.info("資産情報一覧画面遷移処理開始：" + joinPoint.getSignature());
	}

	@AfterReturning("execution(* asset.controller.AssetListController.list(..))")
	public void AssetListLogAfterReturning(JoinPoint joinPoint) {
		log.info("資産情報一覧画面遷移正常終了：" + joinPoint.getSignature());
	}

	@AfterThrowing(value = "execution(* asset.controller.AssetListController.list(..))", throwing = "e")
	public void AssetListLogAfterThrowing(JoinPoint joinPoint, Throwable e) {
		log.error("資産情報一覧画面遷移エラー発生：", e);
		log.error("Signature：" + joinPoint.getSignature());
	}

	@Before("execution(* asset.repository.AssetRepository.findAllByOrderByContractNumDescAssetNumDesc(..))")
	public void AssetListRepositoryLogBefore(JoinPoint joinPoint) {
		log.info("資産情報全取得処理開始：" + joinPoint.getSignature());
	}

	@AfterReturning("execution(* asset.repository.AssetRepository.findAllByOrderByContractNumDescAssetNumDesc(..))")
	public void AssetListRepositoryLogAfterReturning(JoinPoint joinPoint) {
		log.info("資産情報全取得正常終了：" + joinPoint.getSignature());
	}

	@AfterThrowing(value = "execution(* asset.repository.AssetRepository.findAllByOrderByContractNumDescAssetNumDesc(..))", throwing = "e")
	public void AssetListRepositoryLogAfterThrowing(JoinPoint joinPoint, Throwable e) {
		log.error("資産情報全取得エラー発生：", e);
		log.error("Signature：" + joinPoint.getSignature());
	}

	@Before("execution(* asset.controller.AssetListController.indicationSet(..))")
	public void IndicationSetLogBefore(JoinPoint joinPoint) {
		log.info("表示項目設定処理開始：" + joinPoint.getSignature());
	}

	@AfterReturning("execution(* asset.controller.AssetListController.indicationSet(..))")
	public void IndicationSetLogAfterReturning(JoinPoint joinPoint) {
		log.info("表示項目設定正常終了：" + joinPoint.getSignature());
	}

	@AfterThrowing(value = "execution(* asset.controller.AssetListController.indicationSet(..))", throwing = "e")
	public void IndicationSetLogAfterThrowing(JoinPoint joinPoint, Throwable e) {
		log.error("表示項目設定エラー発生：", e);
		log.error("Signature：" + joinPoint.getSignature());
	}

	@Before("execution(* asset.repository.IndicationRepository.getOne(..))")
	public void IndicationRepositoryLogBefore(JoinPoint joinPoint) {
		log.info("表示項目取得処理開始：" + joinPoint.getSignature());
	}

	@AfterReturning("execution(* asset.repository.IndicationRepository.getOne(..))")
	public void IndicationRepositoryLogAfterReturning(JoinPoint joinPoint) {
		log.info("表示項目取得正常終了：" + joinPoint.getSignature());
	}

	@AfterThrowing(value = "execution(* asset.repository.IndicationRepository.getOne(..))", throwing = "e")
	public void IndicationRepositoryLogAfterThrowing(JoinPoint joinPoint, Throwable e) {
		log.error("表示項目取得エラー発生：", e);
		log.error("Signature：" + joinPoint.getSignature());
	}

	@Before("execution(* asset.controller.AddController.init(..))")
	public void AddLogBefore(JoinPoint joinPoint) {
		log.info("資産情報登録画面遷移処理開始：" + joinPoint.getSignature());
	}

	@AfterReturning("execution(* asset.controller.AddController.init(..))")
	public void AddLogAfterReturning(JoinPoint joinPoint) {
		log.info("資産情報登録画面遷移正常終了：" + joinPoint.getSignature());
	}

	@AfterThrowing(value = "execution(* asset.controller.AddController.init(..))", throwing = "e")
	public void AddLogAfterThrowing(JoinPoint joinPoint, Throwable e) {
		log.error("資産情報登録画面遷移エラー発生：", e);
		log.error("Signature：" + joinPoint.getSignature());
	}

	@Before("execution(* asset.controller.AddController.add(..))")
	public void AssetAddBefore(JoinPoint joinPoint) {
		log.info("資産情報登録処理開始：" + joinPoint.getSignature());
	}

	@AfterReturning("execution(* asset.controller.AddController.add(..))")
	public void AssetAddAfterReturning(JoinPoint joinPoint) {
		log.info("資産情報登録正常終了：" + joinPoint.getSignature());
	}

	@AfterThrowing(value = "execution(* asset.controller.AddController.add(..))", throwing = "e")
	public void AssetAddAfterThrowing(JoinPoint joinPoint, Throwable e) {
		log.error("資産情報登録エラー発生：", e);
		log.error("Signature：" + joinPoint.getSignature());
	}

	@Before("execution(* asset.repository.AssetRepository.save(..))")
	public void AssetListRepositoryAddLogBefore(JoinPoint joinPoint) {
		log.info("資産情報DB保存処理開始：" + joinPoint.getSignature());
	}

	@AfterReturning("execution(* asset.repository.AssetRepository.save(..))")
	public void AssetListRepositoryAddLogAfterReturning(JoinPoint joinPoint) {
		log.info("資産情報DB保存正常終了：" + joinPoint.getSignature());
	}

	@AfterThrowing(value = "execution(* asset.repository.AssetRepository.save(..))", throwing = "e")
	public void AssetListRepositoryAddLogAfterThrowing(JoinPoint joinPoint, Throwable e) {
		log.error("資産情報DB保存エラー発生：", e);
		log.error("Signature：" + joinPoint.getSignature());
	}

	@Before("execution(* asset.controller.DetailController.init(..))")
	public void DetailLogBefore(JoinPoint joinPoint) {
		log.info("資産情報詳細画面遷移処理開始：" + joinPoint.getSignature());
	}

	@AfterReturning("execution(* asset.controller.DetailController.init(..))")
	public void DetailLogAfterReturning(JoinPoint joinPoint) {
		log.info("資産情報詳細画面遷移正常終了：" + joinPoint.getSignature());
	}

	@AfterThrowing(value = "execution(* asset.controller.DetailController.init(..))", throwing = "e")
	public void DetailLogAfterThrowing(JoinPoint joinPoint, Throwable e) {
		log.error("資産情報詳細画面遷移エラー発生：", e);
		log.error("Signature：" + joinPoint.getSignature());
	}

	@Before("execution(* asset.repository.AssetRepository.getOne(..))")
	public void AssetListRepositoryGetOneLogBefore(JoinPoint joinPoint) {
		log.info("資産情報取得処理開始：" + joinPoint.getSignature());
	}

	@AfterReturning("execution(* asset.repository.AssetRepository.getOne(..))")
	public void AssetListRepositoryGetOneLogAfterReturning(JoinPoint joinPoint) {
		log.info("資産情報取得正常終了：" + joinPoint.getSignature());
	}

	@AfterThrowing(value = "execution(* asset.repository.AssetRepository.getOne(..))", throwing = "e")
	public void AssetListRepositoryGetOneLogAfterThrowing(JoinPoint joinPoint, Throwable e) {
		log.error("資産情報取得エラー発生：", e);
		log.error("Signature：" + joinPoint.getSignature());
	}

	@Before("execution(* asset.controller.UpdateController.init(..))")
	public void UpdateLogBefore(JoinPoint joinPoint) {
		log.info("資産情報編集画面遷移処理開始：" + joinPoint.getSignature());
	}

	@AfterReturning("execution(* asset.controller.UpdateController.init(..))")
	public void UpdateLogAfterReturning(JoinPoint joinPoint) {
		log.info("資産情報編集画面遷移正常終了：" + joinPoint.getSignature());
	}

	@AfterThrowing(value = "execution(* asset.controller.UpdateController.init(..))", throwing = "e")
	public void UpdateLogAfterThrowing(JoinPoint joinPoint, Throwable e) {
		log.error("資産情報編集画面遷移エラー発生：", e);
		log.error("Signature：" + joinPoint.getSignature());
	}

	@Before("execution(* asset.controller.UpdateController.update(..))")
	public void AssetUpdateBefore(JoinPoint joinPoint) {
		log.info("資産情報編集処理開始：" + joinPoint.getSignature());
	}

	@AfterReturning("execution(* asset.controller.UpdateController.update(..))")
	public void AssetUpdateAfterReturning(JoinPoint joinPoint) {
		log.info("資産情報編集正常終了：" + joinPoint.getSignature());
	}

	@AfterThrowing(value = "execution(* asset.controller.UpdateController.update(..))", throwing = "e")
	public void AssetUpdateAfterThrowing(JoinPoint joinPoint, Throwable e) {
		log.error("資産情報編集エラー発生：", e);
		log.error("Signature：" + joinPoint.getSignature());
	}

	@Before("execution(* asset.service.AssetTimerService.assetTimer(..))")
	public void AssetTimerServiceBefore(JoinPoint joinPoint) {
		log.info("資産情報タイマー処理開始：" + joinPoint.getSignature());
	}

	@AfterReturning("execution(* asset.service.AssetTimerService.assetTimer())")
	public void AssetTimerServiceAfterReturning(JoinPoint joinPoint) {
		log.info("資産情報タイマー処理正常終了：" + joinPoint.getSignature());
	}
}
