package asset.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import asset.entity.AssetEntity;

/**
 * 資産情報のリポジトリ
 * @author s_nakano
 */
public interface AssetRepository extends JpaRepository<AssetEntity, Integer> {
	List<AssetEntity> findAllByOrderByContractNumAscAssetNumAscAssetIdAsc();

	List<AssetEntity> findByExtensionGreaterThanAndEndDateLessThan(int extension, String endDate);

	@Query(value = "select * from asset order by case when contract_num = '' then '1' else '0' end,contract_num,case when asset_num = '' then '1' else '0' end,asset_num,asset_id", nativeQuery = true)
	List<AssetEntity> findAllSort();
}
