package asset.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import asset.entity.UserEntity;

/**
 * ユーザ情報のリポジトリ
 * @author s_nakano
 */
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

}
