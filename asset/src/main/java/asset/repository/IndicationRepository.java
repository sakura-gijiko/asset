package asset.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import asset.entity.IndicationEntity;

/**
 * 表示項目のリポジトリ
 * @author s_nakano
 */
public interface IndicationRepository extends JpaRepository<IndicationEntity, Integer> {
}
