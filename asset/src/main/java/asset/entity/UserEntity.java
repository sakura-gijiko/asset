package asset.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * ユーザ情報のエンティティ
 * @author s_nakano
 */

@Entity
@Table(name = "user_manage")
@Getter
@Setter
public class UserEntity {
	@Id
	@SequenceGenerator(name = "user_id")
	private int userId;
	private String password;
	private boolean assetAuthority;
	private boolean masterAuthority;

}
