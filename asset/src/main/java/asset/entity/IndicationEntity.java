package asset.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * 表示項目のエンティティ
 * @author s_nakano
 */

@Entity
@Table(name = "indication")
@Getter
@Setter
public class IndicationEntity {
	@Id
	private int assetId;
	private boolean contractNum;
	private boolean startDate;
	private boolean endDate;
	private boolean extension;
	private boolean modelNum;
	private boolean makerModel;
	private boolean itemName;
	private boolean num;
	private boolean assetNum;
	private boolean serialNum;
	private boolean owner;
	private boolean membership;
	private boolean remark1;
	private boolean remark2;

}
