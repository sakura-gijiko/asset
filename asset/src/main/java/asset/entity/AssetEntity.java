package asset.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.Setter;

/**
 * 資産情報のエンティティ
 * @author s_nakano
 */
@Entity
@Table(name = "asset")
@Getter
@Setter
public class AssetEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "asset_id_gen")
	@SequenceGenerator(name = "asset_id_gen", sequenceName = "asset_id", allocationSize = 1)
	private int assetId;
	private String contractNum;
	private String startDate;
	private String endDate;
	private int extension;
	private String modelNum;
	private String makerModel;
	private String itemName;
	private int num;
	private String assetNum;
	private String serialNum;
	private String owner;
	private String membership;
	private String remark1;
	private String remark2;
	private String createUserId;
	private LocalDateTime createDate;
	private String updateUserId;
	private LocalDateTime updateDate;
	@Version
	private int version;

}
