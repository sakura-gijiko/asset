package asset.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import asset.entity.AssetEntity;

/**
 * 入力チェック用のクラス
 * @author s_nakano
 *
 */
public final class InputCheckUtil {
	/**
	 * 渡された資産情報の値をチェックする
	 * @param assetList 呼び出し元から渡された資産情報
	 * @return 何行目にどのような問題があったかの入力チェックの結果
	 */
	public static List<String> inputCheck(List<AssetEntity> assetList) {

		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		df.setLenient(false);
		List<String> inputCheck = new ArrayList<String>();
		int i = 1;
		for (AssetEntity asset : assetList) {
			Date sd = null;
			Date ed = null;
			if (!asset.getContractNum().equals("")) {
				if (!asset.getContractNum().matches("[0-9]{4}-?[0-9]{4}-?[0-9]{4}-?[0-9]{4}")) {
					inputCheck.add(String.valueOf(i));
					inputCheck.add("契約番号はXXXX-XXXX-XXXX-XXXXの形式で入力してください。");
				}
			}
			if (!asset.getStartDate().equals("")) {
				try {
					sd = df.parse(asset.getStartDate());
				} catch (Exception e) {
					inputCheck.add(String.valueOf(i));
					inputCheck.add("レンタル開始日はyyyy/MM/ddの形式で入力してください。");
				}
			}
			if (!asset.getEndDate().equals("")) {
				try {
					ed = df.parse(asset.getEndDate());
				} catch (Exception e) {
					inputCheck.add(String.valueOf(i));
					inputCheck.add("レンタル終了予定日はyyyy/MM/ddの形式で入力してください。");
				}
			}
			if (sd != null && ed != null && sd.compareTo(ed) == 1) {
				inputCheck.add(String.valueOf(i));
				inputCheck.add("入力されたレンタル終了予定日がレンタル開始日より前です。");
			}
			if (asset.getExtension() < 0 || 99 < asset.getExtension()) {
				inputCheck.add(String.valueOf(i));
				inputCheck.add("延長は0～99で入力してください。");
			}
			if (!asset.getModelNum().equals("")) {
				if (!asset.getModelNum().matches("[0-9]+") || asset.getModelNum().length() > 8) {
					inputCheck.add(String.valueOf(i));
					inputCheck.add("型番コードは8桁以下の数字を入力してください。");
				}
			}
			if (!asset.getMakerModel().equals("")) {
				if (!asset.getMakerModel().matches("[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]+")) {
					inputCheck.add(String.valueOf(i));
					inputCheck.add("メーカ型番は英数字記号で入力してください。");
				}
			}
			if (asset.getNum() < 0 || 999 < asset.getNum()) {
				inputCheck.add(String.valueOf(i));
				inputCheck.add("数量は0～999で入力してください。");
			}
			if (!asset.getAssetNum().equals("")) {
				if (!asset.getAssetNum().matches("[0-9]+") || asset.getAssetNum().length() > 8) {
					inputCheck.add(String.valueOf(i));
					inputCheck.add("資産番号は8桁以下の数字を入力してください。");
				}
			}
			if (!asset.getSerialNum().equals("")) {
				if (!asset.getSerialNum().matches("[A-Za-z0-9]+")) {
					inputCheck.add(String.valueOf(i));
					inputCheck.add("製造番号は英数字で入力してください。");
				}
			}
			if (!asset.getMembership().equals("")) {
				if (asset.getMembership().length() > 20) {
					inputCheck.add(String.valueOf(i));
					inputCheck.add("所属グループは20文字以下で入力してください。");
				}
			}

			i++;
		}
		return inputCheck;
	}
}
