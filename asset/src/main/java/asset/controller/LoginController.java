package asset.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import asset.entity.UserEntity;
import asset.form.UserForm;
import asset.repository.UserRepository;

/**
 * ログイン画面遷移用のコントローラー
 * @author s_nakano
 */

@Controller
public class LoginController {
	@Autowired
	UserRepository userRepository;

	/**
	 * ログイン画面へ遷移する。
	 * @param session セッション情報
	 * @return ログイン画面のhtmlファイル
	 */
	@RequestMapping("/asset/login")
	public String login(HttpSession session) {
		session.invalidate();
		return "login";
	}

	/**
	 * /assetListにアクセスする。
	 * @param session セッション情報
	 * @param userForm ログイン画面で入力されたユーザ情報
	 * @param model モデルクラス
	 * @return 資産情報一覧画面のhtmlファイル
	 */
	@PostMapping("/asset/top")
	public String check(HttpSession session, UserForm userForm, Model model) {
		UserEntity user = (UserEntity) session.getAttribute("user");

		//セッションにユーザが保存されていなかった場合
		if (user == null) {
			//入力フォームに値が入力されていなかった場合
			if (userForm.getUserId().equals("")) {
				return "redirect:/asset/login";
			}

			//入力フォームに入力されたユーザIDに基づいて、DBからデータを取得
			user = userRepository.getOne(Integer.parseInt(userForm.getUserId()));

			//DBからデータを取得できていてかつ、DBから取得したパスワードと、入力されたパスワードが合致する場合
			if (user != null && user.getPassword().equals(userForm.getPassword())) {

				//ユーザ情報をセッションに保存
				session.setAttribute("user", user);
				return "redirect:/asset/list";
			} else {
				return "redirect:/asset/login";
			}
		} else {
			return "redirect:/asset/list";
		}
	}
}
