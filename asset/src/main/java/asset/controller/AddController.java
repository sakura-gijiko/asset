package asset.controller;

import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import asset.entity.AssetEntity;
import asset.entity.UserEntity;
import asset.repository.AssetRepository;
import asset.repository.IndicationRepository;
import asset.util.InputCheckUtil;

/**
 * 資産情報登録画面遷移用のコントローラー
 * @author s_nakano
 */

@Controller
public class AddController {
	@Autowired
	AssetRepository assetRepository;

	@Autowired
	IndicationRepository indicationRepository;

	/**
	 * 資産情報登録画面へ遷移する。
	 * @param session セッション情報
	 * @return ユーザセッションがない場合、エラー画面のhtmlファイル<br>そうでなければ資産情報登録画面のhtmlファイル
	 */
	@PostMapping("/asset/add")
	public String init(HttpSession session) {
		//セッションから、ログインユーザの情報を取得
		UserEntity user = (UserEntity) session.getAttribute("user");

		//ログインユーザの情報がなかった場合、ログイン画面へと遷移させる
		if (user == null) {
			return "redirect:/asset/error";
		}

		return "add";
	}

	/**
	 * 受け取ったデータをDBに保存する。
	 * @param assetList 登録画面で入力した資産情報
	 * @param session セッション情報
	 * @return 入力チェック結果
	 */
	@PostMapping(value = "/asset/add/register")
	@ResponseBody
	public List<String> add(@RequestBody List<AssetEntity> assetList, HttpSession session) {
		UserEntity user = (UserEntity) session.getAttribute("user");
		List<String> inputCheck = InputCheckUtil.inputCheck(assetList);
		LocalDateTime d = LocalDateTime.now();

		if (inputCheck.size() == 0) {
			for (AssetEntity asset : assetList) {
				asset.setCreateUserId(String.valueOf(user.getUserId()));
				asset.setCreateDate(d);
				asset.setUpdateUserId(String.valueOf(user.getUserId()));
				asset.setUpdateDate(d);
			}
			assetRepository.saveAll(assetList);
		}

		return inputCheck;
	}

	/**
	 * 資産情報一覧画面に遷移する。
	 * @return /assetListにリダイレクトする
	 */
	@GetMapping("/asset/add/comp")
	public String addComp() {
		return "redirect:/asset/list";
	}
}
