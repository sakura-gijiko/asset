package asset.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import asset.entity.AssetEntity;
import asset.entity.IndicationEntity;
import asset.entity.UserEntity;
import asset.form.SearchForm;
import asset.repository.AssetRepository;
import asset.repository.IndicationRepository;

/**
 * 資産情報一覧画面遷移用のコントローラー
 * @author s_nakano
 */

@Controller
public class AssetListController {
	@Autowired
	AssetRepository assetRepository;

	@Autowired
	IndicationRepository indicationRepository;

	/**
	 * 資産管理DBから資産情報を取得し、その情報を資産情報一覧画面へと引き渡す。
	 * @param session セッション情報
	 * @param model モデルクラス
	 * @return ユーザセッションがない場合、/asset/errorにリダイレクト<br>そうでなければ資産情報一覧画面のhtmlファイル
	 */
	@RequestMapping("/asset/list")
	public String list(HttpSession session, Model model) {
		//セッションから、ログインユーザの情報を取得
		UserEntity user = (UserEntity) session.getAttribute("user");

		//ログインユーザの情報がなかった場合、ログイン画面へと遷移させる
		if (user == null) {
			return "redirect:/asset/error";
		}

		//セッションから、前回資産情報一覧画面での検索内容を取得
		SearchForm search = (SearchForm) session.getAttribute("search");

		//検索内容がなかった場合、初期値を格納
		if (search == null) {
			search = new SearchForm();
		}

		//DBから契約番号の降順、また資産番号の降順でデータを取得する
		List<AssetEntity> assetList = assetRepository.findAllSort();

		model.addAttribute("asset", assetList);
		model.addAttribute("indication", indicationRepository.getOne(user.getUserId()));
		model.addAttribute("search", search);
		return "assetList";
	}

	/**
	 * 資産情報一覧画面での検索内容をセッションに保存する
	 * @param session セッション情報
	 * @param search 資産情報一覧画面から受け取った検索内容
	 */
	@PostMapping(value = "/asset/list/search")
	@ResponseBody
	public void searchSet(HttpSession session, @RequestBody SearchForm search) {
		session.setAttribute("search", search);
	}

	/**
	 * 受け取ったデータを表示項目管理DBに保存する。
	 * @param indicationPost 表示項目管理選択画面から受け取った表示項目の表示非表示選択内容
	 * @param session セッション情報
	 * @return 一項目でも表示するようになっているか否か
	 */
	@PostMapping(value = "/asset/indication/set")
	@ResponseBody
	public boolean indicationSet(@RequestBody IndicationEntity indicationPost, HttpSession session) {
		//セッションから、ログインユーザの情報を取得
		UserEntity user = (UserEntity) session.getAttribute("user");
		boolean checked = true;

		if (!indicationPost.isContractNum() && !indicationPost.isStartDate() && !indicationPost.isEndDate()
				&& !indicationPost.isExtension() && !indicationPost.isModelNum() && !indicationPost.isMakerModel()
				&& !indicationPost.isItemName() && !indicationPost.isNum() && !indicationPost.isAssetNum()
				&& !indicationPost.isSerialNum() && !indicationPost.isOwner() && !indicationPost.isMembership()
				&& !indicationPost.isRemark1() && !indicationPost.isRemark2()) {
			checked = false;
		} else {
			//ログインしているユーザの表示項目設定をDBから取得する
			IndicationEntity indication = indicationRepository.getOne(user.getUserId());

			//表示項目設定をモーダルウィンドウから受け取ったデータで上書きし、DBに保存する
			indication.setContractNum(indicationPost.isContractNum());
			indication.setStartDate(indicationPost.isStartDate());
			indication.setEndDate(indicationPost.isEndDate());
			indication.setExtension(indicationPost.isExtension());
			indication.setModelNum(indicationPost.isModelNum());
			indication.setMakerModel(indicationPost.isMakerModel());
			indication.setItemName(indicationPost.isItemName());
			indication.setNum(indicationPost.isNum());
			indication.setAssetNum(indicationPost.isAssetNum());
			indication.setSerialNum(indicationPost.isSerialNum());
			indication.setOwner(indicationPost.isOwner());
			indication.setMembership(indicationPost.isMembership());
			indication.setRemark1(indicationPost.isRemark1());
			indication.setRemark2(indicationPost.isRemark2());
			indicationRepository.save(indication);
		}
		return checked;
	}

	/**
	 * 表示項目管理DBから各項目の表示情報を取得する
	 * @param session セッション情報
	 * @return 各項目の表示情報
	 */
	@GetMapping(value = "/asset/indication/get")
	@ResponseBody
	public List<Boolean> indicationGet(HttpSession session) {
		//セッションから、ログインユーザの情報を取得
		UserEntity user = (UserEntity) session.getAttribute("user");
		List<Boolean> indicationList = new ArrayList<Boolean>();
		IndicationEntity indication = indicationRepository.getOne(user.getUserId());
		indicationList.add(indication.isContractNum());
		indicationList.add(indication.isStartDate());
		indicationList.add(indication.isEndDate());
		indicationList.add(indication.isExtension());
		indicationList.add(indication.isModelNum());
		indicationList.add(indication.isMakerModel());
		indicationList.add(indication.isItemName());
		indicationList.add(indication.isNum());
		indicationList.add(indication.isAssetNum());
		indicationList.add(indication.isSerialNum());
		indicationList.add(indication.isOwner());
		indicationList.add(indication.isMembership());
		indicationList.add(indication.isRemark1());
		indicationList.add(indication.isRemark2());
		return indicationList;
	}
}
