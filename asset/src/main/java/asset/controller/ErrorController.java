package asset.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 資産情報詳細画面遷移用のコントローラー
 * @author s_nakano
 */

@Controller
public class ErrorController {
	@GetMapping("/asset/error")
	public String error() {
		return "error";
	}
}