package asset.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import asset.entity.AssetEntity;
import asset.entity.UserEntity;
import asset.repository.AssetRepository;
import asset.service.AssetTimerService;
import asset.util.InputCheckUtil;

/**
 * 資産情報編集画面遷移用のコントローラー
 * @author s_nakano
 */

@Controller
public class UpdateController {
	@Autowired
	AssetRepository assetRepository;

	/**
	 * 資産情報編集画面へ遷移する。
	 * @param session セッション情報
	 * @param assetIds 資産情報一覧画面から受け取った資産ID
	 * @param model モデルクラス
	 * @return ユーザセッションがない場合、/asset/errorにリダイレクト<br>資産IDが受け渡されなければ/asset/listにリダイレクト<br>そうでなければ資産情報編集画面のhtmlファイル
	 */
	@PostMapping("/asset/update")
	public String init(HttpSession session, String assetIds, Model model) {
		//セッションから、ログインユーザの情報を取得
		UserEntity user = (UserEntity) session.getAttribute("user");

		//ログインユーザの情報がなかった場合、ログイン画面へと遷移させる
		if (user == null) {
			return "redirect:/asset/error";
		}

		List<AssetEntity> assetList = new ArrayList<AssetEntity>();

		//資産一覧画面から資産IDが受け渡されなかった場合、/assetListにアクセスする
		if (assetIds == null) {
			return "redirect:/asset/list";
		}

		String[] ids = assetIds.split(",", 0);

		//資産IDの数だけ処理を行う
		for (String id : ids) {
			//idListに保存された資産IDに基づいて、DBからデータを取得
			assetList.add(assetRepository.getOne(Integer.valueOf(id)));
		}

		//取得した資産情報を資産情報編集画面へ受け渡す
		model.addAttribute("asset", assetList);
		return "update";
	}

	/**
	 * 受け取ったデータをDBに保存する。
	 * @param assetList 編集画面で入力した資産情報
	 * @param session セッション情報
	 * @return 入力チェック結果
	 */
	@PostMapping(value = "/asset/update/edit")
	@ResponseBody
	public List<String> update(@RequestBody List<AssetEntity> assetList, HttpSession session) {
		UserEntity user = (UserEntity) session.getAttribute("user");
		List<String> inputCheck = new ArrayList<>();
		if (AssetTimerService.isAssetTimerActive()) {
			inputCheck.add("現在資産情報のレンタル終了予定日延長処理を実行中です。\nしばらくお待ちください。");
		} else {
			inputCheck = InputCheckUtil.inputCheck(assetList);
			LocalDateTime d = LocalDateTime.now();
			if (inputCheck.size() == 0) {
				for (AssetEntity asset : assetList) {
					asset.setUpdateUserId(String.valueOf(user.getUserId()));
					asset.setUpdateDate(d);
				}
				assetRepository.saveAll(assetList);
			}
		}
		return inputCheck;
	}

	/**
	 * 資産情報一覧画面に遷移する。
	 * @return /assetListにリダイレクトする
	 */
	@GetMapping("/asset/update/comp")
	public String updateComp() {
		return "redirect:/asset/list";
	}
}
