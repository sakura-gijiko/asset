package asset.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

import asset.entity.AssetEntity;
import asset.entity.UserEntity;
import asset.form.SearchForm;
import asset.repository.AssetRepository;
import lombok.extern.slf4j.Slf4j;

/**
 * 資産情報詳細画面遷移用のコントローラー
 * @author s_nakano
 */

@Slf4j
@Controller
public class DetailController {
	@Autowired
	AssetRepository assetRepository;

	/**
	 * 資産情報詳細画面へ遷移する。
	 * @param session セッション情報
	 * @param assetIds 資産情報一覧画面から受け取った資産ID
	 * @param model モデルクラス
	 * @param search 資産情報一覧画面から受け取った検索内容
	 * @return ユーザセッションがない場合、/asset/errorにリダイレクト<br>資産IDが受け渡されなければ/asset/listにリダイレクト<br>そうでなければ資産情報詳細画面のhtmlファイル
	 */
	@PostMapping("/asset/detail")
	public String init(HttpSession session, String assetIds, Model model, SearchForm search) {
		//セッションから、ログインユーザの情報を取得
		UserEntity user = (UserEntity) session.getAttribute("user");

		//ログインユーザの情報がなかった場合、ログイン画面へと遷移させる
		if (user == null) {
			return "redirect:/asset/error";
		}

		//資産情報一覧画面での検索内容をセッションに保存する
		session.setAttribute("search", search);

		List<AssetEntity> assetList = new ArrayList<AssetEntity>();

		//資産一覧画面から資産IDが受け渡されなかった場合、/assetListにアクセスする
		if (assetIds == null) {
			log.error("資産情報が選択されていません。");
			return "redirect:/asset/list";
		}

		String[] ids = assetIds.split(",", 0);

		//資産IDの数だけ処理を行う
		for (String id : ids) {
			//idListに保存された資産IDに基づいて、DBからデータを取得
			assetList.add(assetRepository.getOne(Integer.valueOf(id)));
		}

		//取得した資産情報を資産情報詳細画面へ受け渡す
		model.addAttribute("asset", assetList);
		return "detail";
	}
}