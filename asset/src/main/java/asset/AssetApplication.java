package asset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import asset.service.AssetTimerService;

@SpringBootApplication
@EnableScheduling
public class AssetApplication {

	@Autowired
	AssetTimerService assetTimerService;

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(AssetApplication.class, args);
		AssetApplication app = ctx.getBean(AssetApplication.class);
		app.extensionUpdate(args);
	}

	public void extensionUpdate(String[] args) {
		assetTimerService.assetTimer();
	}
}
