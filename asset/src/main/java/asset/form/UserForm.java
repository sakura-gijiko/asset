package asset.form;

import lombok.Getter;
import lombok.Setter;

/**
 * ユーザ情報のフォーム
 * @author s_nakano
 */
@Getter
@Setter
public class UserForm {
	private String userId;
	private String password;

}
