package asset.form;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 検索情報のフォーム
 * @author s_nakano
 */
@Getter
@Setter
public class SearchForm implements Serializable {
	private String contractNum = "";
	private String startDateFrom = "";
	private String startDateTo = "";
	private String assetNum = "";
	private String endDateFrom = "";
	private String endDateTo = "";
	private int endDateCheckbox = 1;

}
