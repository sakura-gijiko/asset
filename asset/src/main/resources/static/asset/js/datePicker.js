	jQuery.noConflict(); 
	var j$ = jQuery; 

	j$(function() {
      var holidays = {};
      var show_year = 0;
      var parentId = "";
      /*--------------------------------------------------------------*/
      /* 指定月の１か月前から取得月数(３ヶ月)分の祝日データを取得する */
      /*--------------------------------------------------------------*/
      function searchHolidays(year, month) {

			var m = month - 1 ;
		    var y = year ;
			var d = 1 ;
			var monthCount   = 3 ;  /* 取得月数（3か月分） */
			/* 前月が1月より小さくなる場合前年の１２月を指定 */
			if( m < 1 ){
				m = 12;
				y-- ;
			}
			
			m--;
			day = new Date();
			day.setFullYear(y);
    		day.setDate(1);
   			day.setMonth(m);
			
			/* 取得月数分ループ */
			for ( var i=0 ; i < monthCount ; i++ ){
			 	/* 日数分ループ */
				while( m == day.getMonth()) {
					/* calendar.js JsHoliday */
					var isHoliday = JsHoliday(  day.getFullYear() , day.getMonth()+1 , day.getDate() , day.getDay()) ;
					// 祝日である場合=true
					if( isHoliday ){
						// holidaysに追加する
						var holidaysKey =  j$.formatDate.date(day, "yyyyMMdd");
						holidays[holidaysKey] ={ type:0, title:""};
					}	 
				 	/* 日付加算（+１日）*/
				 	day.setTime( day.getTime() + 1000 * 60 * 60 * 24);
	  			}
				/* 月設定 */
				m = day.getMonth(); 

			}
			
      }
  
     function addClearBtn(input) {
    	  setTimeout(function () {
	    	var buttonPane = j$(input).datepicker("widget").find(".ui-datepicker-buttonpane"); 
	    //	var btn = j$('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">クリア</button>');
	    //	btn.unbind("click").bind("click", function () {j$.datepicker._clearDate(input);}); 
	    //	btn.appendTo(buttonPane);
	   },1);
      };
     function setMonth(e){
    	 
     }
     
  	j$.datepicker.regional['ja'] = {
		closeText: '閉じる',
		prevText: '&#x3c;前',
		nextText: '次&#x3e;',
		currentText: '今日',
		 monthNames: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
		monthNamesShort: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
		dayNames: ['日曜日','月曜日','火曜日','水曜日','木曜日','金曜日','土曜日'],
		dayNamesShort: ['日','月','火','水','木','金','土'],
		dayNamesMin: ['日','月','火','水','木','金','土'],
		weekHeader: '週',
		dateFormat: 'yy/mm/dd',
		firstDay: 0,
		isRTL: false,
		buttonText:"カレンダー",
		showMonthAfterYear: true,
		yearSuffix: '年'};
  	j$.datepicker.setDefaults(j$.datepicker.regional['ja']);

    j$(".datepicker").datepicker({
    	showOn: "button",
        buttonImage: "images/btnIconCal.gif",
        showButtonPanel: true,
        buttonImageOnly: true,
        beforeShow :function(input, inst) {
        	addClearBtn(input);
        	parentId = j$(this).closest(".date").attr("id");
            // テキストフィールドに値が入っていればその日付を、入っていなければ今日の日付を取得
            var date = j$(input).datepicker("getDate") || new Date();
            // その月の休日情報を取得
            searchHolidays(date.getFullYear(), date.getMonth()+1);
        },
        dateFormat: "yy/mm/dd", 
          // カレンダーの月が変更されたら呼び出される
        onChangeMonthYear: function(year, month, input) {
        	addClearBtn(this);
		        // その月の休日情報を取得
		        if(show_year != year){
		            searchHolidays(year, month);
		        }  
        },
        beforeShowDay: function(day) {
          var result;
          var holiday = holidays[j$.formatDate.date(day, "yyyyMMdd")];
          if (holiday) {
            result =  [true, "date-holiday"+holiday.type, holiday.title];
          } else {
            switch (day.getDay()) {
              case 0: // 日曜日か？
                result = [true, "date-sunday"];
                break;
              case 6: // 土曜日か？
                result = [true, "date-saturday"];
                break;
              default:
                result = [true, ""];
                break;
            }
          }
          return result;
        },
        onClose:
                function () {
                        var readId = "#" + parentId;
                        var full = "#full_date-" + parentId.split("-")[1];
                           if(j$(readId).find(full).val() != "" ){
	                           var str = j$(readId).find(full).val().split("/");
	                           j$(readId).find(".year").val(str[0]);
	                           j$(readId).find(".month").val(str[1]);
	                           j$(readId).find(".day").val(str[2]);
	                           j$(readId).find(".year").focus();
	                           
	                           j$("input[name='year']").val(str[0]);
	                           j$("input[name='month']").val(str[1]);
	                           j$("input[name='day']").val(str[2]);
	                           
	                           selCalDate(); /* 呼出元JSPメソッド*/
                           }
                   }
      }).ready(
              function() {
                    now = new Date();
                    var nyear = now.getYear();
                    if (nyear < 2000) { nyear += 1900; }
                    searchHolidays(nyear,now.getMonth());
                }  
         );
    
    j$(".year").change(function(){
    	var changeTgt = j$(this).parent();
        str = j$(changeTgt).children(".year").val() + "/" + j$(changeTgt).children(".month").val() + "/" + j$(changeTgt).children(".day").val();
        if(str.length == 2){
        	str = "";
        }
        var full = "#full_date-" + j$(this).closest(".date").attr("id").split("-")[1];
         
        j$(changeTgt).children(full).val(str);
    });
    j$(".month").change(function(){
    	var changeTgt = j$(this).parent().get(0);
        str = j$(changeTgt).children(".year").val() + "/" + j$(changeTgt).children(".month").val() + "/" + j$(changeTgt).children(".day").val();
        if(str.length == 2){
        	str = "";
        }
        var full = "#full_date-" + j$(this).closest(".date").attr("id").split("-")[1];
        j$(changeTgt).children(full).val(str);
    });
    j$(".day").change(function(){
    	var changeTgt = j$(this).parent().get(0);
        str = j$(changeTgt).children(".year").val() + "/" + j$(changeTgt).children(".month").val() + "/" + j$(changeTgt).children(".day").val();
        if(str.length == 2){
        	str = "";
        }
        var full = "#full_date-" + j$(this).closest(".date").attr("id").split("-")[1];
        j$(changeTgt).children(full).val(str);
    });

	j$("img.ui-datepicker-trigger").click( function(e){
        /* hiddenのINPUT値を設定*/
		var className= j$(this).closest("div.date").attr("id");
        var $this = j$("#"+className);
        var date = $this.find(".year").val() +"/"+$this.find(".month").val() +"/" +$this.find(".day").val();
        $this.find("input[name='fromDate']").val(date);
    	
	});
  });
  
  j$(function() {
      var holidays = {};
      var show_year = 0;
      var parentId = "";
      

      function searchHolidays(year, month) {

            return false;
      }
      
      function addClearBtn(input) {
    	  setTimeout(function () {
	    	var buttonPane = j$(input).datepicker("widget").find(".ui-datepicker-buttonpane"); 
	    	//var btn = j$('<button class="ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all" type="button">クリア</button>');
	    	//btn.unbind("click").bind("click", function () {j$.datepicker._clearDate(input); j$(input).parent("div").children(".full_dateTime").val("");}); 
	    	//btn.appendTo(buttonPane);
	   },1);
      };
      
    	j$.datepicker.regional['ja'] = {
    			closeText: '閉じる',
    			prevText: '&#x3c;前',
    			nextText: '次&#x3e;',
    			currentText: '今日',
    			 monthNames: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
    			monthNamesShort: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
    			dayNames: ['日曜日','月曜日','火曜日','水曜日','木曜日','金曜日','土曜日'],
    			dayNamesShort: ['日','月','火','水','木','金','土'],
    			dayNamesMin: ['日','月','火','水','木','金','土'],
    			weekHeader: '週',
    			dateFormat: 'yy/mm/dd',
    			firstDay: 0,
    			isRTL: false,
    			showMonthAfterYear: true,
    			yearSuffix: '年'};
    j$.datepicker.setDefaults( j$.datepicker.regional[ "ja" ] );
    j$(".datetimepicker").datepicker({
    	showOn: "button",
        buttonImage: "images/btnIconCal.gif",
        showButtonPanel: true,
        buttonImageOnly: true,
        beforeShow :function(input, inst) {
        	addClearBtn(input);
        	parentId = j$(this).parent().get(0).id;
            // テキストフィールドに値が入っていればその日付を、入っていなければ今日の日付を取得
            var date = j$(input).datepicker("getDate") || new Date();
            // その月の休日情報をajaxで取得
            searchHolidays(date.getFullYear(), date.getMonth()+1);
        },
        dateFormat: "yy/mm/dd", 
          // カレンダーの月が変更されたら呼び出される
          onChangeMonthYear: function(year, month, inst) {
        	  addClearBtn(this);
            // その月の休日情報を取得
        if(show_year != year){
            searchHolidays(year, month);
        }
            
          },
        beforeShowDay: function(day) {
          var result;
          var holiday = holidays[j$.formatDate.date(day, "yyyyMMdd")];
          if (holiday) {
            result =  [true, "date-holiday"+holiday.type, holiday.title];
          } else {
            switch (day.getDay()) {
              case 0: // 日曜日か？
                result = [true, "date-sunday"];
                break;
              case 6: // 土曜日か？
                result = [true, "date-saturday"];
                break;
              default:
                result = [true, ""];
                break;
            }
          }
          return result;
        },
        onClose:
                function () {
                        var readId = "#" + parentId;
                        var full = "#full_date_time-" + parentId.split("-")[1];
                           var str = j$(readId).find(full).val().split("/");
                           j$(readId).find(".year_dt").val(str[0]);
                           j$(readId).find(".month_dt").val(str[1]);
                           j$(readId).find(".day_dt").val(str[2]);
                           j$(readId).find(".full_dateTime").val(j$(readId).find(full).val() + " " +j$(readId).find(".full_time").val());
                           j$(readId).find(".year_dt").focus();
                   }
      }).ready(
              function() {
                    now = new Date();
                    var nyear = now.getYear();
                    if (nyear < 2000) { nyear += 1900; }
                    searchHolidays(nyear,now.getMonth());
                }  
         );
    
    j$(".year_dt").change(function(){
    	var changeTgt = j$(this).parent().get(0);
        str = j$(changeTgt).children(".year_dt").val() + "/" + j$(changeTgt).children(".month_dt").val() + "/" + j$(changeTgt).children(".day_dt").val();
        if(str.length == 2){
        	str = "";
        }
        var full = "#full_date_time-" + j$(this).parent().get(0).id.split("-")[1];
        j$(changeTgt).children(full).val(str);
        str2 = j$(changeTgt).children(".full_time").val();
        if(str == "" && str2 == ""){
        	 j$(changeTgt).children(".full_dateTime").val("");
        }else{
            j$(changeTgt).children(".full_dateTime").val(str + " " +str2);
        }
    });
    j$(".month_dt").change(function(){
    	var changeTgt = j$(this).parent().get(0);
        str = j$(changeTgt).children(".year_dt").val() + "/" + j$(changeTgt).children(".month_dt").val() + "/" + j$(changeTgt).children(".day_dt").val();
        if(str.length == 2){
        	str = "";
        }
        var full = "#full_date_time-" + j$(this).parent().get(0).id.split("-")[1];
        j$(changeTgt).children(full).val(str);
        str2 = j$(changeTgt).children(".full_time").val();
        if(str == "" && str2 == ""){
        	 j$(changeTgt).children(".full_dateTime").val("");
        }else{
            j$(changeTgt).children(".full_dateTime").val(str + " " +str2);
        }
    });
    j$(".day_dt").change(function(){
    	var changeTgt = j$(this).parent().get(0);
        str = j$(changeTgt).children(".year_dt").val() + "/" + j$(changeTgt).children(".month_dt").val() + "/" + j$(changeTgt).children(".day_dt").val();
        if(str.length == 2){
        	str = "";
        }
        var full = "#full_date_time-" + j$(this).parent().get(0).id.split("-")[1];
        j$(changeTgt).children(full).val(str);
        str2 = j$(changeTgt).children(".full_time").val();
        if(str == "" && str2 == ""){
        	 j$(changeTgt).children(".full_dateTime").val("");
        }else{
            j$(changeTgt).children(".full_dateTime").val(str + " " +str2);
        }
    });
  });
  
  