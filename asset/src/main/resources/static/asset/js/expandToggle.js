/*
 * 要素の表示/非表示切替処理
 */
(function($) {
	$.fn.contentsExpande = function(target, options) {
		var $elems = this;
		// optionをマージ
		var opts = $.extend({}, $.fn.contentsExpande.defaults, options);
		// 要素にexpandToggleメソッドを追加
		$elems.each(function() {
			this.expandToggle = function() {
				var $this = $(this);
				var expanded = $this.hasClass(opts.expandClass);
				
				var ope = expanded ? "show" : "hide";
				$(target).each(function() {
					if (opts.expandWidth && opts.expandHeight) {
						$(this).animate({ width: ope, height: ope }, opts.speed);
					} else if (opts.expandWidth && !opts.expandHeight) {
						$(this).animate({ width: ope}, opts.speed);
					} else if (!opts.expandWidth && opts.expandHeight) {
						$(this).animate({ height: ope }, opts.speed);
					}
				});
			};
		});
		
		$elems.click(function() {
			var $this = $(this);
			if (opts.closeOther && !$this.hasClass(opts.expandClass)) {
				// 他を閉じる
				$("." + opts.expandClass).each(function() {
					$(this).toggleClass(opts.expandClass);
					this.expandToggle();
				});
			}
			// 要素のクラスを切替
			$elems.each(function() {
				$(this).toggleClass(opts.expandClass);
			});
			this.expandToggle();
		});
		
		return this;
	};
	
	$.fn.contentsExpande.defaults = {
		expandClass: "expandted",
		speed: 'slow',
		expandWidth: true,
		expandHeight: true,
		closeOther: true
	};

}) (jQuery);
