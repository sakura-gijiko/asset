//右クリック禁止
document.oncontextmenu = function(){return false};


//指定ミリ秒待つ
function sleep(milsec) {
	endTime = new Date().getTime() + milsec;
	nowTime = new Date().getTime();
	while(nowTime < endTime) {
		nowTime = new Date().getTime();
	}
}

//ログアウトする
function login() {
	removeCookie('Login_employee_cd');
	document.forms['frm'].selectedButton.value = "";
	document.forms['frm'].action = "login.do";
	document.forms['frm'].submit();
}

function logout() {
	removeCookie('Login_employee_cd');
	document.forms['frm'].selectedButton.value = "logout";
	document.forms['frm'].action = "menu.do";
	document.forms['frm'].submit();
}

//ページの移動
function movePage(page) {
	document.forms['frm'].selectedButton.value = "";
	document.forms['frm'].action = page + ".do";
	document.forms['frm'].submit();
}

//ページの移動とアクション指定
function movePageAndSelected(page, selected) {
	document.forms['frm'].selectedButton.value = selected;
	document.forms['frm'].action = page + ".do";
	document.forms['frm'].submit();
}

//閏年チェック
function checkLeapYear(year) {
	if(year%4 == 0 && (year%100!=0 || year%400==0)) {
		return true;
	}
	return false;
}

//日付の存在チェック
function checkDay(year,month,day) {
 if (isNaN(year)) return false;
 if (isNaN(month)) return false;
 if (isNaN(day)) return false;

	year = Number(year);
	month = Number(month);
	day = Number(day);
	
	if(month == 2) {
		if(checkLeapYear(year)) {
			if(day < 1 || 29 < day) {
				return false;
			}
		} else {
			if(day < 1 || 28 < day) {
				return false;
			}
		}
	} else if(month == 4 || month == 6 || month == 9 || month == 11) {
		if(day < 1 || 30 < day) {
			return false;
		}
	} else {
		if(day < 1 || 31 < day) {
			return false;
		}
	}
	return true;
}

//年月日の入力チェック
function checkYMD(yearElm, monthElm, dayElm, yearDsp, monthDsp, dayDsp) {
	year = yearElm.value;
	month = monthElm.value;
	day = dayElm.value;
	// 年のチェック
	if(year == "") {
		alert(yearDsp + "を入力してください。");
		yearElm.focus();
		return false;
	}
	if(year.match(/[^1234567890]/)) {
		alert(yearDsp + "には数値を入力してください。");
		yearElm.focus();
		return false;
	}
	year = Number(year);
	if(year < 2000 || 2100 < year) {
		alert(yearDsp + "は2000年から2100年の範囲で入力してください。");
		yearElm.focus();
		return false;
	}
	
	// 月のチェック}
	if(month == "") {
		alert(monthDsp + "を入力してください。");
		monthElm.focus();
		return false;
	}
	if(month.match(/[^1234567890]/)) {
		alert(monthDsp + "には数値を入力してください。");
		monthElm.focus();
		return false;
	}
	month = Number(month);	
	if(month < 1 || 12 < month) {
		alert(monthDsp + "は1月から12月の範囲で入力してください。");
		monthElm.focus();
		return false;
	}
	
	// 日のチェック
	if(day == "") {
		alert(dayDsp + "を入力してください。");
		dayElm.focus();
		return false;
	}
	if(day.match(/[^1234567890]/)) {
		alert(dayDsp + "には数値を入力してください。");
		dayElm.focus();
		return false;
	}
	day = Number(day);
	if(!checkDay(year,month,day)) {
		alert(dayDsp + "は存在しない日付です。");
		dayElm.focus();
		return false;
	}
	
	return true;
}

//文字列のバイト数を取得する
function getByteLen(str) {
    byteLen = 0;
    for(i=0;i<str.length;i++) {
        c = str.charCodeAt(i);
        // Shift_JIS: 0x0 ～ 0x80, 0xa0  , 0xa1   ～ 0xdf  , 0xfd   ～ 0xff
        // Unicode  : 0x0 ～ 0x80, 0xf8f0, 0xff61 ～ 0xff9f, 0xf8f1 ～ 0xf8f3
        if ( (c >= 0x0 && c < 0x81) || (c == 0xf8f0) || (c >= 0xff61 && c < 0xffa0) || (c >= 0xf8f1 && c < 0xf8f4) ) {
            byteLen += 1;
        } else {
            byteLen += 2;
        }
    }
    return byteLen;
}

//禁止文字チェック
function checkNoUseString(str, inputName) {
	if(str.match(/[－～∥―｜]/)) {
		alert(inputName + "に－,～,∥,―,｜は使用できません。");
		return false;
	}
	return true;
}

//文字列がスペースのみかチェック
function checkSpaceValue(valueElm){
	var str = valueElm.value;
	if( (str!="") && (!str.match(/[^ 　]/)) ){
		alert("半角または全角スペースのみでの登録はできません。");
		valueElm.value = "";
		valueElm.focus();
		return false;
	}
	return true;
}

//文字列の前後の空白を削除する
function trim(str){
	return str.replace(/^[ 　]+|[ 　]+$/g, "");
}

//画面内のinput-fileを全てdisabledにする
function setInputFileDisabled() {
	oInputElms = document.getElementsByTagName("input");
	for(i=0;i<oInputElms.length;i++) {
		if(oInputElms[i].type == "file") {
			oInputElms[i].disabled = true;
		}
	}
}

//画面内のinput-fileを全てdisabled解除にする
function setInputFileNotDisabled() {
	oInputElms = document.getElementsByTagName("input");
	for(i=0;i<oInputElms.length;i++) {
		if(oInputElms[i].type == "file") {
			oInputElms[i].disabled = false;
		}
	}
}


/******************************************************************************************/
/* 【数字】                                                                               */
/* 0, 1, 2, 3, 4, 5, 6, 7, 8, 9                                                           */
/******************************************************************************************/
function checkHankakuSuuji(checkStr){

	var i;
	var code;

	for (i=0; i<checkStr.length; i++) {
	
		code = checkStr.charCodeAt(i);
		if ( (48 <= code && code <= 57) ) {
			;
		} else {
			return false;
		}
    }

	return true;
}
 

/******************************************************************************************/
/* 【半角英数字(E-mail仕様)】                                                             */
/* 0, 1, 2, 3, 4, 5, 6, 7, 8, 9                                                           */
/* A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z           */
/* a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z           */
/* -, ., _                                                                                */
/******************************************************************************************/
function checkHankakuEiSuuji(checkStr){

	var i;
	var code;
	
	for (i=0; i<checkStr.length; i++) {
	
		code = checkStr.charCodeAt(i);
		if ( (48 <= code && code <=57) || (65<= code && code <=90) ||
			 (97 <= code && code <= 122) || (45 == code || 46 == code || 95 == code) ){
			;
		}else{
			return false;
		}
    }
    
	return true;
}


/******************************************************************************************/
/* 【全角カナチェック】                                                                   */
/* ァ, ア, ィ, イ, ゥ, ウ, ェ, エ, ォ, オ, カ, ガ, キ, ギ, ク, グ, ケ, ゲ, コ, ゴ,        */
/* サ, ザ, シ, ジ, ス, ズ, セ, ゼ, ソ, ゾ, タ, ダ, チ, ヂ, ッ, ツ, ヅ, テ, デ, ト, ド,    */
/* ナ, ニ, ヌ, ネ, ノ, ハ, バ, パ, ヒ, ビ, ピ, フ, ブ, プ, ヘ, ベ, ペ, ホ, ボ, ポ,        */
/* マ, ミ, ム, メ, モ, ャ, ヤ, ュ, ユ, ョ, ヨ, ラ, リ, ル, レ, ロ, ヮ, ワ, ヰ, ヱ, ヲ, ン,*/
/* ヴ, ヵ, ヶ, ・, ー, ヽ, ヾ                                                             */
/******************************************************************************************/
function checkZenkakuKana(checkStr){

	var i;
	var code;
	
	for (i=0; i<checkStr.length; i++) {
	
    	code = checkStr.charCodeAt(i);
      	if ( (12449 <= code && code <= 12542) ){
			;
		} else {
			return false;
		}
    }
    
	return true;
}

/******************************************************************************************/
/* 【アンチ半角カナチェック】                                                             */
/* ｦ, ｧ, ｨ, ｩ, ｪ, ｫ, ｬ, ｭ, ｮ, ｯ, ｰ, ｱ, ｲ, ｳ, ｴ, ｵ, ｶ, ｷ, ｸ, ｹ, ｺ, ｻ, ｼ, ｽ, ｾ, ｿ, ﾀ, ﾁ, ﾂ, */
/* ﾃ, ﾄ, ﾅ, ﾆ, ﾇ, ﾈ, ﾉ, ﾊ, ﾋ, ﾌ, ﾍ, ﾎ, ﾏ, ﾐ, ﾑ, ﾒ, ﾓ, ﾔ, ﾕ, ﾖ, ﾗ, ﾘ, ﾙ, ﾚ, ﾛ, ﾜ, ﾝ, ﾞ, ﾟ  */
/******************************************************************************************/
function checkAntiHankakuKana(checkStr){

	var i;
	var code;
	
	for (i=0; i<checkStr.length; i++) {
	
    	code = checkStr.charCodeAt(i);
      	if ( (65382 <= code && code <= 65439) ){
			return true;
		}
    }
    
	return false;

}

function removeCookie(name) {
	var time = new Date();
	time.setFullYear(time.getFullYear() - 1);
	var expire = "expires=" + time.toGMTString() + ";";
	document.cookie = name + "=;" + expire;
} 
function openPetiInfo(id, ev) {
	var oElm 	= document.getElementById(id) ;
	var widthBox	= 400;
	var heightBox	= 290;
	
	var pointX 	= ev.clientX - widthBox/2 ;
	if( pointX < 0 ){
		pointX = 0;
	}else if(pointX > document.body.clientWidth - widthBox ){
		pointX = document.body.clientWidth - widthBox ;
	}

	var pointY = ev.clientY- (heightBox+15) ;
	if( pointY < 0) {
		pointY = 0;
	}
	
	oElm.style.top	= pointY+"px";
	oElm.style.left	= pointX+"px";
	new Effect.Appear(id, { from:0.5, to:1.0, duration:0.5 });

}

function closePetiInfo(id) {
	new Effect.Fade(id, { from:0.5, to:0.0 , duration:0.8 });
}

function openAppearBox(id, ev, x, y) {
	var oElm 	= document.getElementById(id) ;
	oElm.style.top	= y + "px";
	oElm.style.left	= x + "px";
	new Effect.Appear(id, { from:0.5, to:1.0, duration:0.5 });
}

function closeAppearBox(id) {
	new Effect.Fade(id, { from:0.3, to:0.0 , duration:0.3 });
}
