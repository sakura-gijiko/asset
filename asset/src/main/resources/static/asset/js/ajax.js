var oXMLHttpRequest;

//XMLHttpRequestオブジェクト作成
function createXMLHttpRequestObject() {
	try {
		oXMLHttpRequest = new XMLHttpRequest();
	} catch(e) {
		try {
			oXMLHttpRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch(e) {
			try {
				oXMLHttpRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch(e) {
			}
		}
	}
}

//Ajaxでリクエストデータ送信
function sendRequestAjax(action, prmStr, callFunc) {
	if(document.forms['frm'].submitCheck.value == "1") {
		return;
	}
	if(oXMLHttpRequest == null) {
		createXMLHttpRequestObject();
	}
	oXMLHttpRequest.open("POST", action);
	oXMLHttpRequest.setRequestHeader("content-type", "application/x-www-form-urlencoded;charset=UTF-8");
	if(callFunc) {
		oXMLHttpRequest.onreadystatechange = callFunc;
	}
	document.forms['frm'].submitCheck.value = "1";
	oXMLHttpRequest.send(prmStr);
}

//パラメータ文字列追加
function addPrmStrAjax(prmStr, prmName, prmValue) {
	if(prmStr == null) {
		prmStr = "";
	}
	if(prmStr.length > 0) {
		prmStr += "&";
	}
	prmStr += prmName + "=" + encodeURIComponent(prmValue);
	return prmStr;
}

//Ajaxでレスポンスデータの受信が完了しているか判別する
function isResponseDataReceivedAjax() {
	var b = false;
	if(oXMLHttpRequest.readyState == 4 && oXMLHttpRequest.status == 200) {
		b = true;
		document.forms['frm'].submitCheck.value = "0";
	}
	return b;
}

//Ajaxで受信したレスポンスがテキスト形式か確認
function isResponseTextAjax() {
	var b = false;
	var contentType = oXMLHttpRequest.getResponseHeader("Content-Type");
	if(contentType.match(/text\/plain/i)) {
		b = true;
	}
	return b;
}

//Ajaxで受信したレスポンスがXML形式か確認
function isResponseXMLAjax() {
	var b = false;
	var contentType = oXMLHttpRequest.getResponseHeader("Content-Type");
	if(contentType.match(/text\/xml/i)) {
		b = true;
	}
	return b;
}

//テキスト形式でレスポンスデータ取得
function getResponseTextAjax() {
	return oXMLHttpRequest.responseText;
}

//XML形式でレスポンスデータ取得
function getResponseXMLAjax() {
	return oXMLHttpRequest.responseXML;
}

//XMLタグのテキスト取得
function getXMLTagText(oXMLTag) {
	var text = "";
	if(oXMLTag.childNodes.length > 0) {
		text = oXMLTag.childNodes[0].nodeValue;
	}
	return text;
}

//HTMLのセレクトタグのオプションを入れ替える（オプション用のXML指定）
function replaceSelectOptionWithXMLOptions(oSelect, oXMLOptions) {
	removeAllSelectOption(oSelect);
	
	for(var i=0;i<oXMLOptions.length;i++) {
		var optionValue = getXMLTagText(oXMLOptions[i].getElementsByTagName("value")[0]);
		var optionText = getXMLTagText(oXMLOptions[i].getElementsByTagName("text")[0]);
		addSelectOption(oSelect, optionValue, optionText);
	}
}

//HTMLのセレクトタグのオプションを入れ替える
function replaceSelectOption(oSelect) {
	var oXML = getResponseXMLAjax();
	var oXMLOptions = oXML.getElementsByTagName("option");
	replaceSelectOptionWithXMLOptions(oSelect, oXMLOptions);
}

//HTMLのセレクトタグのオプションを入れ替える（親のタグ指定）
function replaceSelectOptionWithParentTagName(oSelect, parentTagName) {
	var oXML = getResponseXMLAjax();
	var oXMLParentOption = oXML.getElementsByTagName(parentTagName)[0];
	var oXMLOptions = oXMLParentOption.getElementsByTagName("option");
	replaceSelectOptionWithXMLOptions(oSelect, oXMLOptions);
}

//HTMLのセレクトタグのオプションを全て削除
function removeAllSelectOption(oSelect) {
	while(oSelect.options.length > 0) {
		oSelect.options.remove(0);
	}
}

//HTMLのセレクトタグのオプションを空のみにする
function setSelectOptionOnlyEmpty(oSelect) {
	removeAllSelectOption(oSelect);
	addSelectOption(oSelect, "", "");
}

//HTMLのセレクトタグにオプションを追加する
function addSelectOption(oSelect, optionValue, optionText) {
	var oOption = document.createElement("option");
	oOption.value = optionValue;
	oOption.text = optionText;
	oSelect.options.add(oOption);
}
