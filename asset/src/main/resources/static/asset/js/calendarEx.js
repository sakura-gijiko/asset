// カレンダーウインドウハンドル
var winCalendar = null;
/*****************************************************************************/
/*  月名称描画                                                               */
/*  説明：月名称を出力する                                                   */
/*  入力：y = 年、m = 月                                                     */
/*  出力：なし                                                               */
/*****************************************************************************/
function  JsWriteCalendarTitle( y, m )
{
    ms  = new Array("1", "2", "3", "4",
                    "5", "6", "7", "8", "9",
                    "10", "11", "12");

    // 指定年月の月末日を取得
    d  = JsGetDays(y,m+1);

    // 月２桁化
    if(m < 9)
        m2 = "0" + (m+1);
    else
        m2 = (m+1);
    // 日付２桁化
    if((d > 0) && (d < 10))
        d2 = "0" + d;
    else
        d2 = d;

    HtmlData = "<TD height=24px NOWRAP ALIGN=center style='font-size:18px;'><A HREF=\"JavaScript:JSMonthSet('" + y + "/" + m2 + "/" + d2 + "')\" TARGET=button><FONT COLOR=#0D5693>";
    HtmlData += y + " <span style=font-size:14px>年</span>" + "  " + ms[m] + " <span style=font-size:14px>月</span>";
    HtmlData += "</FONT></A></TD>";

    return HtmlData;
}

/*****************************************************************************/
/*  曜日文字編集                                                             */
/*  説明：曜日を編集する                                                     */
/*  入力：size = 曜日の下の区切り線のピクセル値                              */
/*  出力：編集データ                                                         */
/*****************************************************************************/
function JsWriteDayOfWeek( size )
{
    swd  = new Array("日", " 月", " 火", " 水", " 木", " 金", " 土");
    rgb = new Array("#CC3333", "#000000", "#000000",

                    "#000000", "#000000", "#000000", "#3366FF");

    HtmlData = "<TR><td COLSPAN=7><table style='solid;border-bottom:1px #eeeeee solid;' cellspacing=0 cellpadding=0  width=100% height=24px bgcolor=#f8f8f8><tr>";
    for(i = 0; i < 7; i++)
        HtmlData += "<TD NOWRAP align=center>" + swd[i].fontcolor(rgb[i]) + "</TD>";
    HtmlData += "</TR></table></td></tr>";
//    HtmlData += "<TR><TD NOWRAP COLSPAN=7><img src=images/spacer.gif height=2></TD><TR>";
    return HtmlData;
}

/*****************************************************************************/
/*                                                           */
/*  説明：                                                   */
/*  入力：y = 年、m = 月                                                     */
/*  出力：なし                                                               */
/*****************************************************************************/
function JsGetDays( y, m )
{
    var d;

    if(m == 2){
        d = 28 + (y % 4 == 0) - (y % 100 == 0) + (y % 400 == 0);
    }
    else{
        if(m < 8)
            d = 30 + (m % 2);
        else
            d = 31 - (m % 2);
    }
    return d;
}

/*****************************************************************************/
/*  休日かどうかのチェック                                                   */
/*  説明：                               */
/*  入力：y = , m = , d = , w= 日付                                          */
/*  出力：true/false                                                         */
/*****************************************************************************/
function  JsHoliday( y, m, d, w )
{
	var IDX_0915 = 9;
	var IDX_0923 = 10;
	var IDX_1010 = 11;
	if (y >= 2016) {
    	hd = new Array("101","115","211","321","429","503","504",
                "505","720","811", "915","923","1010","1103","1123","1223", "0");
    	IDX_0915 = 10;
    	IDX_0923 = 11;
    	IDX_1010 = 12;
	} else {
    	hd = new Array("101","115","211","321","429","503","504",
                   "505","720","915","923","1010","1103","1123","1223", "0");
	}

    // 春分／秋分の日を求める（1980-2099まで）
    hd[3]  = "3" + Math.floor(20.8431 + 0.242194 * (y - 1980) - Math.floor((y - 1980)/4));
    hd[IDX_0923] = "9" + Math.floor(23.2488 + 0.242194 * (y - 1980) - Math.floor((y - 1980)/4));

    // 2000年以降の成人の日/体育の日(第２月曜日)の計算
    temp = new Date(y + "/" + m + "/01");
    startDay = temp.getDay();
    if(y >= 2000 &&(m == 1 || m == 10)){
        if(startDay <= 1)  happyMon = 7 + (2 - startDay);
        else               happyMon = 14 - (startDay - 2);
        if(happyMon < 10)  happyMon = "0" + happyMon;
        if(m == 1)         hd[1] = "01" + happyMon;
        else               hd[IDX_1010] = "10" + happyMon;
    }
    // 2003年以降の海の日/敬老の日(第３月曜日)の計算
    temp = new Date(y + "/" + m + "/01");
    startDay = temp.getDay();
    if(y >= 2003 &&(m == 7 || m == 9)){
        if(startDay <= 1)  happyMon = 1 + (1 - startDay) + 14;
        else               happyMon = 8 - (startDay - 1) + 14;
        if(happyMon < 10)  happyMon = "0" + happyMon;
        if(m == 7)         hd[8] = "07" + happyMon;
        else               hd[IDX_0915] = "09" + happyMon;
    }

    h = m * 100 + d;
    for(i = 0; i < 20 && h >= hd[i]; i++){
        if(h == hd[i])
            return true;
    }

    //祝日に挟まれた平日のチェック
    yesterday = h;
    tomorrow = h;
    if(d == 1){
      if(m == 1){
        yesterday = 12 * 100 + JsGetDays(y, 12);
      }else{
        yesterday = (m - 1) * 100 + JsGetDays(y, m - 1);
      }
    }else{
      yesterday -= 1;
    }
    for(i = 0; i < 20 && yesterday >= hd[i]; i++){
      if(yesterday == hd[i]){
        if(d == JsGetDays(y, m)){
          if(m == 12){
            tomorrow = 1 * 100 + 1;
          }else{
            tomorrow = (m + 1) * 100 + 1;
          }
        }else{
          tomorrow += 1;
        }
        for(i = 0; i < 20 && tomorrow >= hd[i]; i++){
          if(tomorrow == hd[i]){
            return true;
          }
        }
      }
    }

    //振り替え休日チェック
    if(w < 6){
      while(w > 0){
        yHlFlg = false;
        if(d == 1){
          if(m == 1){
            m = 12;
            h = m * 100 + JsGetDays(y, m);
          }else{
            m = m - 1;
            h = m * 100 + JsGetDays(y, m);
          }
          d = JsGetDays(y, m);
        }else{
          h -= 1;
          d -= 1;
        }
        for(i = 0; i < 20 && h >= hd[i]; i++){
          if(h == hd[i]){
            yHlFlg = true;
            if((w-1) == 0){
              return true;
            }else{
              break;
            }
          }
        }
        w -= 1;
        if(!yHlFlg){
          break;
        }
      }
    }

    return false;
}

/*****************************************************************************/
/*  ｶﾚﾝﾀﾞｰ日付部描画                                                         */
/*  説明：日付部を出力する                                                   */
/*  入力：y = 年、m = 月                                                     */
/*  出力：なし                                                               */
/*****************************************************************************/
function JSDayOut(y, m)
{
    var  m2, d2;

    // 曜日表示
    HtmlData = JsWriteDayOfWeek("2");

    day = new Date();
    today = new Date();
    // 指定年月の１日
    day.setFullYear(y);
    day.setDate(1);
    day.setMonth(m);
    // 日曜日から１日の曜日までブランク
    for(i = 0; i < day.getDay(); i++)
        HtmlData += "<TD NOWRAP></TD>";
    // 指定月で繰り返す
    while(m == day.getMonth()){
        // 日曜日になったら次の行
        if(day.getDay() == 0)
            HtmlData += "<TR>";
        // 日付・曜日
        d = day.getDate();
        w = day.getDay();
        Col = d.toString();
        // 月２桁化
        if(m < 9)
            m2 = "0" + (m+1);
        else
            m2 = (m+1);
        // 日付２桁化
        if((d > 0) && (d < 10))
            d2 = "0" + d;
        else
            d2 = d;

        // 今日は色を変える
        if(d == today.getDate() && y == today.getFullYear() && m == today.getMonth())
            HtmlData += "<TD NOWRAP ALIGN=center><U>" + "<A HREF=\"JavaScript:JSDateSet('" + y + "/" + m2 + "/" + d2 + "')\" TARGET=button><b>" + Col.fontcolor("#339933") + "</b></A></TD>\n";
        // 日曜日は色を変える
        else if(JsHoliday(day.getFullYear(), day.getMonth() + 1, d, w) == true)
            HtmlData += "<TD NOWRAP ALIGN=center>" + "<A HREF=\"JavaScript:JSDateSet('" + y + "/" + m2 + "/" + d2 + "')\" TARGET=button>" + Col.fontcolor("FF6600") + "</A></TD>\n";
        else
            HtmlData += "<TD NOWRAP ALIGN=center>" + "<A HREF=\"JavaScript:JSDateSet('" + y + "/" + m2 + "/" + d2 + "')\" TARGET=button>" + Col.fontcolor(rgb[w]) + "</A></TD>\n";
        // 日付を進める
        day.setTime(day.getTime() + 1000 * 60 * 60 * 24);
    }
    return HtmlData;
}

/*****************************************************************************/
/*  月別カレンダー表示                                                       */
/*  説明：月別カレンダーを出力する                                           */
/*  入力：count（現在月からの増分）                                          */
/*  出力：なし                                                               */
/*****************************************************************************/
function MonthMove(count)
{
    // 今月１日
    day  = new Date();
    day.setDate("1");
    // 当月　＋　増分
    day.setMonth(day.getMonth() + count);
    // y = year
    y = day.getFullYear();
    // m = month
    m = day.getMonth();

HtmlData  = "<HTML><HEAD><TITLE></TITLE>\n";
HtmlData  +="<script language='JavaScript' src='js/calendar.js'></script>"; /* ←この記述がないとChromeきかない*/
HtmlData  += "<script type='text/javascript'>\n";
HtmlData  += "<!--\n";
HtmlData  += "document.oncontextmenu = function(){return false};\n";
HtmlData  += "//-->\n";
HtmlData  += "</script>\n";

HtmlData  += "<STYLE type=text/css>\n";
HtmlData  += "<!--\n";
HtmlData  += "td{ font-size=16px;} \n";
HtmlData  += "A:link{ text-decoration:none;} \n";
HtmlData  += "A:visited{ text-decoration:none;} \n";
HtmlData  += "-->\n";
HtmlData  += "</STYLE></HEAD>\n";

HtmlData += "<BODY BGCOLOR=#ffffff TEXT=#000000 topmargin=0 leftmargin=10 >\n";
HtmlData += "<BASEFONT FACE=MS UI Gothic >\n";
HtmlData += "<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=1 WIDTH=292>\n";
HtmlData += "<TR><TD BGCOLOR=#dddddd>\n";

HtmlData += "<TABLE BORDER=0 CELLSPACING=0 CELLPADDING=5 WIDTH=290 background=images/bgCalMain.jpg>\n";
HtmlData += "<TR><TD NOWRAP ALIGN=center>\n";

HtmlData += "<TABLE style='border-top:1px solid #cccccc;border-left:1px solid #cccccc;border-right:1px solid #f0f0f0;border-bottom:1px solid #f0f0f0;'  BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH=90% BGCOLOR=#ffffff>\n";
HtmlData += JsWriteCalendarTitle(y, m);
HtmlData += "</TABLE>\n";

HtmlData += "</TD></TR>\n";
HtmlData += "<TR>\n";
HtmlData += "<TD NOWRAP ALIGN=center >\n";

HtmlData += "<TABLE style='border-top:1px solid #cccccc;border-left:1px solid #cccccc;border-right:1px solid #f0f0f0;border-bottom:1px solid #f0f0f0;' BGCOLOR=#ffffff CELLSPACING=0 CELLPADDING=1 WIDTH=90%>\n";
HtmlData += JSDayOut(y, m);
HtmlData += "</TABLE>\n";

HtmlData += "</TD></TR>\n";
HtmlData += "</TABLE>\n";

HtmlData += "</TD></TR>\n";
HtmlData += "</TABLE>\n";

HtmlData += "<TABLE CELLSPACING=0 CELLPADDING=0 WIDTH=292><TR><TD ALIGN=right><img src=images/btnCalFoot.jpg border=0></TD></TR></TABLE>\n";
HtmlData += "</BODY>\n";
HtmlData += "</HTML>\n";

    top.result.document.open("text/html");
    top.result.document.write(HtmlData);
    top.result.document.close();
}

/*****************************************************************************/
/*  月別カレンダー表示制御                                                   */
/*  説明：月別カレンダーを出力する                                           */
/*  入力：Kind( 0:現在月の表示、1:前の月表示、2:次の月表示                   */
/*  出力：なし                                                               */
/*****************************************************************************/
function JsCalendarControl(Kind)
{
    // カウント（現在月からの増分）
    form = top.button.document.calendar;
    count = form.count.value;
    if(Kind == 0) {
        // 出力対象項目名取得
        form.itemyy.value = top.opener.document.calendar.itemyy.value;
        form.itemmm.value = top.opener.document.calendar.itemmm.value;
        form.itemdd.value = top.opener.document.calendar.itemdd.value;
        count = 0;
    }
    else if(Kind == 1) {
        count--;
    }
    else {
        count++;
    }
    // カレンダー再表示
    MonthMove(count);
    form.count.value = count;
}

/*****************************************************************************/
/*  クリック時の日付設定                                                     */
/*  説明：クリック時に呼び出され日付を記憶する                               */
/*  入力：Date = 日付                                                        */
/*  出力：なし                                                               */
/*****************************************************************************/
function JSMonthSet(Data)
{
    var itemyy;
    var itemmm;
    var itemdd;
    var date_str = Data.split("/");

    // 呼出元へデータを設定し、閉じる
	top.opener.document.forms['frm'].year.value = date_str[0];
	top.opener.document.forms['frm'].month.value = date_str[1];
	top.opener.document.forms['frm'].day.value = date_str[2];

	top.close();
	top.opener.selCalMonth();

}

/*****************************************************************************/
/*  クリック時の日付設定                                                     */
/*  説明：クリック時に呼び出され日付を記憶する                               */
/*  入力：Date = 日付                                                        */
/*  出力：なし                                                               */
/*****************************************************************************/
function JSDateSet(Data)
{
    var itemyy;
    var itemmm;
    var itemdd;
    var date_str = Data.split("/");

    // 呼出元へデータを設定し、閉じる
//    item1 = eval("top.opener.document." + top.button.document.calendar.item1.value);
//    item1.value = date_str[0];
//    item2 = eval("top.opener.document." + top.button.document.calendar.item2.value);
//    item2.value = date_str[1];
//    item3 = eval("top.opener.document." + top.button.document.calendar.item3.value);
//    item3.value = date_str[2];

	top.opener.document.forms['frm'].year.value = date_str[0];
	top.opener.document.forms['frm'].month.value = date_str[1];
	top.opener.document.forms['frm'].day.value = date_str[2];

	top.close();
	top.opener.selCalDate();

/*
    itemyy = eval("top.opener.document." + top.button.document.calendar.itemyy.value);
    itemyy.value = date_str[0];
    itemmm = eval("top.opener.document." + top.button.document.calendar.itemmm.value);
    itemmm.value = date_str[1];
    itemdd = eval("top.opener.document." + top.button.document.calendar.itemdd.value);
    itemdd.value = date_str[2];

    itemyy.focus();
    top.close();
*/
}

/*****************************************************************************/
/*  カレンダーウインドウ出力(年月日)                                         */
/*  説明：カレンダーウインドウを出力する                                     */
/*  入力：form = 出力form名（出力エレメント名は"caldate"固定）               */
/*  出力：なし                                                               */
/*****************************************************************************/
function JsCalendar( date_yy,data_mm,data_dd )
{
    document.calendar.itemyy.value = date_yy;
    document.calendar.itemmm.value = data_mm;
    document.calendar.itemdd.value = data_dd;

    // カレンダーを開く
    winCalendar = window.open
            ("calendarEx_open.html", "calendar", "width=315,height=260,toolbar=no,menubar=no,scrollbars=yes,resizable=no,status=no,top=150,left=300");

    winCalendar.focus();
//    winCalendar.location = "calendarEx_open.html";
}