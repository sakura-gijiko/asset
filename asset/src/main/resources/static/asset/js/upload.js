function upload(url1,url2){
	try{
		var ai = document.forms[0].asset_id;

		//入力フォームから契約番号を取得
		var cn = document.forms[0].contract_num;

		//入力フォームからレンタル開始日を取得
		var sd = document.forms[0].start_date;

		//入力フォームからレンタル終了予定日を取得
		var ed = document.forms[0].end_date;

		var e = document.forms[0].extension;

		//入力フォームから型番コードを取得
		var mn = document.forms[0].model_num;

		//入力フォームからメーカ型番を取得
		var mm = document.forms[0].maker_model;

		//入力フォームから商品名を取得
		var itn = document.forms[0].item_name;

		//入力フォームから数量を取得
		var n = document.forms[0].num;

		//入力フォームから資産番号を取得
		var an = document.forms[0].asset_num;

		//入力フォームから製造番号を取得
		var sn = document.forms[0].serial_num;

		//入力フォームから所有者
		var o = document.forms[0].owner;

		//入力フォームから所属グループ
		var ms = document.forms[0].membership;

		//入力フォームから備考1を取得
		var r1 = document.forms[0].remark1;

		//入力フォームから備考2を取得
		var r2 = document.forms[0].remark2;

		var c = document.forms[0].create_user_id;

		var cd = document.forms[0].create_date;

		var v = document.forms[0].version;

		//JSON形式のデータを保存する変数
		var json1=new Array();

		//aiが配列だった場合、ai.valueは空文字となるため、aiが配列かを識別
		if(cn.value==""){

			//入力された行の数だけ処理を行う
			for(var i=0;i<cn.length;i++){

				//JSON形式で保存
				json1.push({
					"assetId":ai[i].value,
					"contractNum":cn[i].value,
					"startDate":sd[i].value,
					"endDate":ed[i].value,
					"extension":e[i].value,
					"modelNum":mn[i].value,
					"makerModel":mm[i].value,
					"itemName":itn[i].value,
					"num":n[i].value,
					"assetNum":an[i].value,
					"serialNum":sn[i].value,
					"owner":o[i].value,
					"membership":ms[i].value,
					"remark1":r1[i].value,
					"remark2":r2[i].value,
					"createUserId":c[i].value,
					"createDate":cd[i].value,
					"version":v[i].value,
				});
			}
		}else{
			//JSON形式で保存
			json1.push({
				"assetId":ai.value,
				"contractNum":cn.value,
				"startDate":sd.value,
				"endDate":ed.value,
				"extension":e.value,
				"modelNum":mn.value,
				"makerModel":mm.value,
				"itemName":itn.value,
				"num":n.value,
				"assetNum":an.value,
				"serialNum":sn.value,
				"owner":o.value,
				"membership":ms.value,
				"remark1":r1.value,
				"remark2":r2.value,
				"createUserId":c.value,
				"createDate":cd.value,
				"version":v.value,
			});
		}

		//ajaxでjsonデータを送信する
		$.ajax({
			url:url1,
			type:"POST",
			contentType: "application/json",
			data:JSON.stringify(json1),
			dataType:"json"
			}).done(function(inputChecked,textStatus,jqXHR) {
				if(Object.entries(inputChecked)==""){
					//現在の日付を取得
					var startMsec = new Date();

					// 指定ミリ秒間だけループさせる
					while (new Date() - startMsec < 500);

					// /update_compにアクセスする
					window.location.href = url2;
				}else if(inputChecked.length==1){
					alert(inputChecked[0]);
				}else{
					for(var i=0;i<inputChecked.length;i=i+2){
						alert(inputChecked[i]+"行目："+inputChecked[i+1]);
					}
				}
			}).fail(function(jqXHR, textStatus, errorThrown){
				alert("jqXHR.status:"+jqXHR.status+"\ntextStatus:"+textStatus+"\nerrorThrown"+errorThrown);
			}).always(function(){
			});

	}catch (e) {
		//受け取ったエラーメッセージを表示する
		alert(e);
	}
}