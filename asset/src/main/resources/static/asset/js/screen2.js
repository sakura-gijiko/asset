function changeScreenSize(){
/*
	var ua = navigator.userAgent;
	var nWidth, nHeight;
	var nHit = ua.indexOf("MSIE");
	var bIE = (nHit >=  0);
	var bVer6 = (bIE && ua.substr(nHit+5, 1) == "6");
	var bStd = (document.compatMode && document.compatMode=="CSS1Compat");
	if (bIE) {
		if (bVer6 && bStd) {
			nWidth = document.documentElement.clientWidth;
			nHeight = document.documentElement.clientHeight;
		} else {
			nWidth = document.body.clientWidth;
			nHeight = document.body.clientHeight;
		}
	} else {
		nWidth = window.innerWidth;
		nHeight = window.innerHeight;
	}
*/
	nWidth = document.documentElement.clientWidth;
	nHeight = document.documentElement.clientHeight;
	
	chgDivSize(nWidth, nHeight);
}

// 画面解像度に対するDIVサイズ算出
function setScrollDiv(idname,hfix,h0,wfix,w0){
	var obj = document.getElementById(idname) ;
	if(hfix>0 && h0>=hfix){
		obj.style.height = (h0-hfix).toString()+"px";
	}
	if(wfix>0 && w0>=wfix){
		obj.style.width  = (w0-wfix).toString()+"px";
	}
}
function setScrollDivW(idname,wfix,w0){
	var obj = document.getElementById(idname) ;
	if(wfix>0 && w0>=wfix){
		obj.style.width  = (w0-wfix).toString()+"px";
	}
}

function setScrollDivH(idname,hfix,h0){
	var obj = document.getElementById(idname) ;
	if(hfix>0 && h0>=hfix){
		obj.style.height = (h0-hfix).toString()+"px";
	}
}

// 解像度に対するcontentサイズ算出
function setContentDiv(h0){
	var obj = document.getElementById("content") ;
	if (h0 >= 150){
		obj.style.height = (h0-150).toString()+"px";
	}
}

function setDiv(idname,h0,w0){
	var obj = document.getElementById(idname);
	obj.style.height = (h0).toString()+"px";
	obj.style.width  = (w0).toString()+"px";
}
function setDivW(idname,w0){
	var obj = document.getElementById(idname);
	obj.style.width  = (w0).toString()+"px";
}
function setDivH(idname,h0){
	var obj = document.getElementById(idname);
	obj.style.height = (h0).toString()+"px";
}

//スクリーンサイズにあわせたTextAreaのrowsを算出
///rowsに画面サイズ(高さ)が768pxの時に指定したい値を設定
function setTextAreaRows(idname,rows,h0){
	var obj = document.getElementById(idname);
	var fixSize = Math.floor(rows * 14.15);
	obj.rows = Math.floor((h0-(768- fixSize +2)) / 14.15);
}

// 解像度に対するcontentサイズおよびDIVサイズ算出
function setContentScrollDiv(idname,hfix,h0){
	var idobj = document.getElementById(idname);
	idobj.style.height = (h0-hfix-150).toString()+"px";
}

//解像度に対するcontentサイズ算出
function setContentDivH(h0){
	var obj = document.getElementById("content") ;
	obj.style.height = (h0-150).toString()+"px";
}
