// ポップアップ表示スクリプト（タイトル表示無し）
// 
// onClick="showDetailClick('表示文字列');" 
// onMouseOver="showDetail('表示文字列');" 
// onMouseOut="javascript:hideChkDIV();"
//
// ※ 必須ファイル「onmouseHelp.js」

var flgClickOpen = false ;

function showDetail( ev ,cmt){
	if(flgClickOpen) return;
	var tag;
	tag='<div class="showPopup" id="showPopupBox">';
	tag+='<a href="javascript:closeDIV();"><img src="images/btnCloseBox.gif" border="0" alt="閉じる" style="margin-top:3px;" align="right"></a>';
	tag+='&nbsp';
	if(cmt != "") {
	 tag+='<div class="showNote"><textarea cols="50" rows="5" readonly>'+cmt+'</textarea></div>';
	 tag+='</div>';
	}
	showTAGids( ev ,tag,'showPopupBox');
	return true;
}

function showDetailClick( ev , cmt){
	flgClickOpen = false;
	showDetail(ev , cmt);
	flgClickOpen = true;
	return true;
}

function hideChkDIV(){
	if(flgClickOpen) return;
	hideDIV();
	return true ;
}

function closeDIV(){
	flgClickOpen = false ;
	hideDIV();
}