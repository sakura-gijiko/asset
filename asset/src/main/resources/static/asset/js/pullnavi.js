function openPull(pullID) {
	document.getElementById(pullID).style.display = "block";
}

function closePull(pullID) {
	document.getElementById(pullID).style.display = "none";
}

function changeColor(oTD) {
	oTD.style.backgroundColor = "#2E7BC9";
	oTD.style.color = "#ffffff";
}

function returnColor(oTD) {
	oTD.style.backgroundColor = "#1F5286";
	oTD.style.color = "#ffffff";
}
